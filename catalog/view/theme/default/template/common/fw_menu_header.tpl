<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/fw_menu/perfect-scrollbar/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/fw_menu/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/fw_menu.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/fw_menu/fw_menu.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php echo $google_analytics; ?>
</head>
<body class="<?php echo $class; ?>">
<nav id="top">
  <div class="container">
    <?php echo $currency; ?>
    <?php echo $language; ?>
    <div id="top-links" class="nav pull-right">
      <ul class="list-inline">
        <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-5"><?php echo $search; ?>
      </div>
      <div class="col-sm-3"><?php echo $cart; ?></div>
    </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav categories navbar-nav">
        <?php foreach ($categories as $category) {
            $hasproducts = '';
            if ($category['products_inside']) $hasproducts="hasproducts"; ?>
            <?php if ($category['products_inside'] || $category['children']) { ?>
        	<li>
        		<a class="fw_category <?php echo $hasproducts;?>" id="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></a>
	      		<button type="button" class="navbar-showall btn btn-navbar"><i class="fa fa-plus"></i></button>
        	</li>
        	<?php } ?>
        <?php } ?>
      </ul>
    <?php foreach ($categories as $category) { ?>
		<?php if ($category['children']) { ?>
        <div class="nav subnav" id="subnav-<?php echo $category['id'];?>">
	      <ul class="nav subcategories navbar-nav">
            <?php foreach ($category['children'] as $child) {
	            $hasproducts = '';
	            if ($child['products_inside']) { ?>
            	<li>
            		<a class="fw_subcategory hasproducts" id="<?php echo $child['id']; ?>" data-parentid="<?php echo $category['id'];?>"><?php echo $child['name']; ?></a>
		      		<button type="button" class="subnavbar-showall btn btn-navbar"><i class="fa fa-plus"></i></button>
        		</li>
                <?php if ($child['children']) { ?>
                <li class="subsubnav" id="subsubnav-<?php echo $child['id'];?>">
                    <ul class="nav subsubcategories navbar-nav">
                        <?php foreach ($child['children'] as $grand_child) {
                            $hasproducts = '';
                            if ($grand_child['products_inside']) $hasproducts="hasproducts"; ?>
                        <li><a class="fw_subsubcategory <?php echo $hasproducts;?>" id="<?php echo $grand_child['id']; ?>" data-parentid="<?php echo $child['id'];?>"><?php echo $grand_child['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
            	<?php } ?>
	        <?php } ?>
	      </ul>
	    </div>
	    <?php } ?>
    <?php } ?>
    <!--<div class="nav_products_container">-->
    <div class="nav products" id="product-list">
    	<span id="productLoading"></span>
        <ul class="nav products navbar-nav">
        </ul>
    </div>
    <!--<div id="products_left_arrow"><img src="catalog/view/theme/default/image/arrow-left.png"></div>
    <div id="products_right_arrow"><img src="catalog/view/theme/default/image/arrow-right.png"></div>-->
    <!--</div>-->
  </nav>
</div>
<?php } ?>
