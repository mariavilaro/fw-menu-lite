$(document).ready(function() {

	$('#product-list').perfectScrollbar({suppressScrollY:true});
	/**
	 * AJAX load all the products in a category 
 	 * @param string categoryId - category id to load
	 */
	function loadProducts(categoryId) {
		var ajax_data='category=' + categoryId;
		$('#product-list ul').html('');
		$('#product-list').show();
		showProductLoading(true);
		$.ajax({
			url: 'index.php?route=module/fw_menu/',
			type: 'post',
			data: ajax_data,
			dataType: 'json'
		})
		.done(function(json) {
			for (i=0;i<json['data'].length;i++) {
				$('#product-list ul').append('<li><a href="'+json['data'][i]['href']+'"><img class="img img-responsive" src="'+json['data'][i]['image']+'" title="'+json['data'][i]['name']+'" /></a></li>');
			}
			$('#product-list').data('cat',categoryId);
			$('#product-list').scrollLeft(0);

			//little hack to let the images render and their container expands to full size before updating the scrollbar
			//should find something that fires an event when #product-list ul size changes
			setTimeout(function(){ $('#product-list').perfectScrollbar('update'); },1000);
			
		})
		.fail(function() {
			$('#product-list').hide();
			console.log('Error receiving products, make sure fw_menu.php is in controller/module folder');
		})
		.always(function() {
			showProductLoading(false);
		});
	}

	/**
	 * Click in first level category 
	 */
	$('#menu').find('.fw_category').click(function(event) {
		event.preventDefault();
		id = $(this)[0].id;
		$('.fw_category').removeClass('active');
		$('.fw_subcategory').removeClass('active');
		$('.fw_subsubcategory').removeClass('active');
		$('.fw_subcategory').parent('li').removeClass('responsive-show');
		$('.fw_subcategory').parent('li').removeClass('responsive-hide');
		$('.fw_category').parent('li').removeClass('responsive-show');
		$('.fw_category').parent('li').addClass('responsive-hide');
		$(this).parent('li').removeClass('responsive-hide');
		$(this).parent('li').addClass('responsive-show');
		$(this).addClass('active');
		hasproducts = $(this).hasClass('hasproducts');
		$('#menu').find('.subnav').hide();
		$('#menu').find('.subsubnav').hide();
		$('#product-list').hide();
		$('#subnav-'+id).show();
		if (hasproducts) {
			loadProducts(id);
		}
	});

	$('.navbar-showall').click(function() {
		$('.fw_category').parent('li').removeClass('responsive-hide');
		$(this).parent('li').removeClass('responsive-show');
	});

	/**
	 * Click in second level category
	 */
	$('#menu').find('.fw_subcategory').click(function(event) {
		event.preventDefault();
		id = $(this)[0].id;
		parentid = $(this).data('parentid');
		$('.fw_category').each(function(elem){
			if ($(this).attr('id') != parentid) {
				$(this).parent('li').addClass('responsive-hide');
			} else {
				$(this).parent('li').addClass('responsive-show');
			}
		});
		$('.fw_subcategory').removeClass('active');
		$('.fw_subsubcategory').removeClass('active');
		$('.fw_subcategory').parent('li').removeClass('responsive-show');
		$('.fw_subcategory').parent('li').addClass('responsive-hide');
		$(this).parent('li').removeClass('responsive-hide');
		$(this).parent('li').addClass('responsive-show');
		$(this).addClass('active');
		hasproducts = $(this).hasClass('hasproducts');
		$('#menu').find('.subsubnav').hide();
		$('#product-list').hide();
		$('#subsubnav-'+id).show();
		if (hasproducts) {
			loadProducts(id);
		}
	});

	$('.subnavbar-showall').click(function() {
		$('.fw_subcategory').parent('li').removeClass('responsive-hide');
		$(this).parent('li').removeClass('responsive-show');
		//$('#menu').find('.subsubnav').hide();
	});

	/**
	 * Click in third level category
	 */
	$('#menu').find('.fw_subsubcategory').click(function(event) {
		event.preventDefault();
		id = $(this)[0].id;
		parentid = $(this).data('parentid');
		grandparentid = $('#'+parentid).data('parentid');
		$('.fw_category').each(function(elem){
			if ($(this).attr('id') != grandparentid) {
				$(this).parent('li').addClass('responsive-hide');
			} else {
				$(this).parent('li').addClass('responsive-show');
			}
		});
		$('.fw_subcategory').each(function(elem){
			if ($(this).attr('id') != parentid) {
				$(this).parent('li').addClass('responsive-hide');
			} else {
				$(this).parent('li').addClass('responsive-show');
			}
		});
		$('.fw_subsubcategory').removeClass('active');
		$(this).addClass('active');
		hasproducts = $(this).hasClass('hasproducts');
		$('#product-list').hide();
		if (hasproducts) {
			loadProducts(id);
		}
	});
   
	function showProductLoading(show) {
		$loading = $('#productLoading');
		if (show) {
			$loading.show();
		} else $loading.hide();
	}

});