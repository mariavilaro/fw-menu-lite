<?php
/**
 * AJAX Product loading for fw_menu
 */
class ControllerModuleFWMenu extends Controller {
    /**
     * AJAX Product loading
     * @param POST category - category id
     * @return JSON {result:0 (error) / 1 (ok), data: product array}
     */
    public function index() {
        $return=array('result'=>1,'data'=>array());
        
        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        if (!$this->config->get('config_fw_menu_product_width')) {
            $this->config->set('config_fw_menu_product_width', 180);
            $this->config->set('config_fw_menu_product_height', 180);
        }

        if (isset($this->request->post['category'])) {
            $category = $this->request->post['category'];

            $filter_data = array(
                'filter_category_id' => $category
            );

            $results = $this->model_catalog_product->getProducts($filter_data);

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_fw_menu_product_width'), $this->config->get('config_fw_menu_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_fw_menu_product_width'), $this->config->get('config_fw_menu_product_height'));
                }
                $href = $this->url->link('product/product', '&product_id=' . $result['product_id']);

                $return['data'][] = array(
                    'product_id'    => $result['product_id'],
                    'name'          => $result['name'],
                    'href'          => $href,
                    'image'         => $image,
                );
            }
        }
        echo json_encode($return);
    }
}