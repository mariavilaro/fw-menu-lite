Flexible menu

Show category products in the header menu

- by: flexiblewebs.net
- version: 1.2
- for: Opencart 2.0.3
- requires: VQMod 2.5.1

This work is licensed under a Creative Commons Attribution 4.0 International License.
http://creativecommons.org/licenses/by/4.0/

# README #

This is a VQMod module for Opencart 2.

This module changes the Opencart main menu functionality. It shows 2 levels of categories, and if the selected category has products in it, they are loaded in a scrollable carousel.

It supports 3 levels of categories (for now!)

If a category doesn't have products or categories in it, it's not shown.

On mobile, the not selected level 1 and 2 categories are collapsed, and they can be shown again tapping the + sign next to the selected category.

Update! Now with responsive image sizes. The plugin default is 180px for desktops and 80px for mobile (using css media queries)

See the demo here:

http://flexiblewebs.net/projects/opencart/

See the Opencart Extension page here:

http://opencart.com/index.php?route=extension/extension/info&extension_id=22754

### How do I get set up? ###

Install:

VQMod required!!

You need to install VQMod first
https://github.com/vqmod/vqmod/wiki/Installing-vQmod-on-OpenCart

Copy all contents in 'upload' folder into your store root folder. No files will be replaced.

That's all! Your new menu is working.

You can change the "Flexible Menu Image Size" settings (in your store settings -> Image) to the max size you wish to show.

To uninstall, just delete the file fw_menu.xml from vqmod/xml folder

### Changelog ###

1.2 - 	Responsive image sizes.
    - 	New setting in Store Settings -> Image.
    - 	In mobile, always hide expanded categories when a subcategory is selected.

1.1 - 	3 category levels supported.

1.0 - 	First version. 2 category levels supported.